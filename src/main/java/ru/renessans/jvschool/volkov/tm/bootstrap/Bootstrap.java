package ru.renessans.jvschool.volkov.tm.bootstrap;

import ru.renessans.jvschool.volkov.tm.api.controller.ICommandController;
import ru.renessans.jvschool.volkov.tm.api.controller.ICrudController;
import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.api.view.ICommandView;
import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.constant.CmdConst;
import ru.renessans.jvschool.volkov.tm.controller.CommandController;
import ru.renessans.jvschool.volkov.tm.controller.ProjectController;
import ru.renessans.jvschool.volkov.tm.controller.TaskController;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.repository.CommandRepository;
import ru.renessans.jvschool.volkov.tm.repository.ProjectRepository;
import ru.renessans.jvschool.volkov.tm.repository.TaskRepository;
import ru.renessans.jvschool.volkov.tm.service.CommandService;
import ru.renessans.jvschool.volkov.tm.service.ProjectService;
import ru.renessans.jvschool.volkov.tm.service.TaskService;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;
import ru.renessans.jvschool.volkov.tm.view.CommandView;
import ru.renessans.jvschool.volkov.tm.view.ProjectView;
import ru.renessans.jvschool.volkov.tm.view.TaskView;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandView commandView = new CommandView();

    private final ICommandController commandController = new CommandController(commandService, commandView);

    private final ICrudRepository<Task> taskRepository = new TaskRepository();

    private final ICrudService<Task> taskService = new TaskService(taskRepository);

    private final ICrudView<Task> taskView = new TaskView();

    private final ICrudController taskController = new TaskController(taskService, taskView);

    private final ICrudRepository<Project> projectRepository = new ProjectRepository();

    private final ICrudService<Project> projectService = new ProjectService(projectRepository);

    private final ICrudView<Project> projectView = new ProjectView();

    private final ICrudController projectController = new ProjectController(projectService, projectView);

    public Bootstrap() {
        this.commandController.registrationCommandTypes();
        this.commandController.registrationCommands();
        this.commandController.registrationNotifyCommands();
    }

    public void run(String... args) {
        final boolean emptyArgs = ValidRuleUtil.isNullOrEmpty(args);
        if (emptyArgs) terminalCommandPrintLoop();
        else argumentPrint(args);
    }

    private void terminalCommandPrintLoop() {
        String command = "";
        while (!CmdConst.EXIT.equals(command)) {
            command = ScannerUtil.nextLine();
            this.commandController.printCommandNotify(command);
            executeTaskOrProject(command);
        }
    }

    private void argumentPrint(String... args) {
        final String arg = args[0];
        this.commandController.printArgumentNotify(arg);
    }

    private void executeTaskOrProject(final String factor) {
        if (ValidRuleUtil.isNullOrEmpty(factor)) return;

        switch (factor) {
            case CmdConst.TASK_CREATE:
                this.taskController.create();
                break;
            case CmdConst.TASK_LIST:
                this.taskController.list();
                break;
            case CmdConst.TASK_CLEAR:
                this.taskController.removeAll();
                break;
            case CmdConst.PROJECT_CREATE:
                this.projectController.create();
                break;
            case CmdConst.PROJECT_LIST:
                this.projectController.list();
                break;
            case CmdConst.PROJECT_CLEAR:
                this.projectController.removeAll();
                break;
            case CmdConst.TASK_UPDATE_BY_INDEX:
                this.taskController.updateByIndex();
                break;
            case CmdConst.TASK_UPDATE_BY_ID:
                this.taskController.updateById();
                break;
            case CmdConst.TASK_DELETE_BY_INDEX:
                this.taskController.removeByIndex();
                break;
            case CmdConst.TASK_DELETE_BY_ID:
                this.taskController.removeById();
                break;
            case CmdConst.TASK_DELETE_BY_TITLE:
                this.taskController.removeByTitle();
                break;
            case CmdConst.TASK_VIEW_BY_INDEX:
                this.taskController.viewByIndex();
                break;
            case CmdConst.TASK_VIEW_BY_ID:
                this.taskController.viewById();
                break;
            case CmdConst.PROJECT_UPDATE_BY_INDEX:
                this.projectController.updateByIndex();
                break;
            case CmdConst.PROJECT_UPDATE_BY_ID:
                this.projectController.updateById();
                break;
            case CmdConst.PROJECT_DELETE_BY_INDEX:
                this.projectController.removeByIndex();
                break;
            case CmdConst.PROJECT_DELETE_BY_ID:
                this.projectController.removeById();
                break;
            case CmdConst.PROJECT_DELETE_BY_TITLE:
                this.projectController.removeByTitle();
                break;
            case CmdConst.PROJECT_VIEW_BY_INDEX:
                this.projectController.viewByIndex();
                break;
            case CmdConst.PROJECT_VIEW_BY_ID:
                this.projectController.viewById();
                break;
            default:
                break;
        }
    }

}