package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.model.Project;

import java.util.*;

public final class ProjectRepository implements ICrudRepository<Project> {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        if (Objects.isNull(project)) return;
        this.projects.add(project);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = getByIndex(index);
        if (Objects.isNull(project)) return null;
        this.projects.remove(project);
        return project;
    }

    @Override
    public Project removeById(final String id) {
        final Project project = getById(id);
        if (Objects.isNull(project)) return null;
        this.projects.remove(project);
        return project;
    }

    @Override
    public Project removeByTitle(final String title) {
        final Project project = getByTitle(title);
        if (Objects.isNull(project)) return null;
        this.projects.remove(project);
        return project;
    }

    @Override
    public void removeAll() {
        this.projects.clear();
    }

    @Override
    public Project getByIndex(final Integer index) {
        return this.projects.get(index);
    }

    @Override
    public Project getById(final String id) {
        for (final Project project : this.projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project getByTitle(final String name) {
        for (final Project project : this.projects) {
            if (name.equals(project.getTitle())) return project;
        }
        return null;
    }

    @Override
    public List<Project> getAll() {
        return new ArrayList<>(this.projects);
    }

}