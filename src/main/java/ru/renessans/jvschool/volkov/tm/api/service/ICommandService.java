package ru.renessans.jvschool.volkov.tm.api.service;

public interface ICommandService {

    void addCommandTypes();

    void addCommonCommands();

    void addTerminalCommands();

    void addArgumentCommands();

    void addNotifyCommonCommands();

    void addNotifyTerminalCommands();

    void addNotifyArgumentCommands();

    String getTerminalCommandNotify(String command);

    String getArgumentCommandNotify(String command);

}