package ru.renessans.jvschool.volkov.tm.enumeration;

public enum CommandType {

    COMMON, CMD, ARG;

    public boolean isCommon() {
        return this == COMMON;
    }

    public boolean isCommand() {
        return this == CMD;
    }

    public boolean isArgument() {
        return this == ARG;
    }

}