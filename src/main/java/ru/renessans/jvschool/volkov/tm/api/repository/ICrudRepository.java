package ru.renessans.jvschool.volkov.tm.api.repository;

import java.util.List;

public interface ICrudRepository<T> {

    void add(T object);

    T removeByIndex(Integer integer);

    T removeById(String id);

    T removeByTitle(String title);

    void removeAll();

    T getByIndex(Integer index);

    T getById(String id);

    T getByTitle(String title);

    List<T> getAll();

}