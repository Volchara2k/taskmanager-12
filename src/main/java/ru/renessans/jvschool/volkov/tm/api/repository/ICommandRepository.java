package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.model.Command;

import java.util.List;

public interface ICommandRepository {

    void addCommandTypes(CommandType... commandTypes);

    void addCommands(CommandType commandType, Command... commands);

    List<Command> getCommandsByType(final CommandType commandType);

    String getNotifyByType(CommandType commandType, String command);

}