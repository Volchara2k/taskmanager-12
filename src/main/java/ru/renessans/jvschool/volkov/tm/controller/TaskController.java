package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.ICrudController;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.model.Task;

import java.util.List;

public final class TaskController implements ICrudController {

    private final ICrudService<Task> taskService;

    public final ICrudView<Task> taskView;

    public TaskController(ICrudService<Task> taskService, ICrudView<Task> taskView) {
        this.taskService = taskService;
        this.taskView = taskView;
    }

    @Override
    public void create() {
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        this.taskService.add(title, description);
        this.taskView.print(NotifyConst.SUCCESS_MSG);
    }

    @Override
    public void list() {
        final List<Task> tasks = this.taskService.getAll();
        this.taskView.print(tasks);
    }

    @Override
    public void updateByIndex() {
        final String indexLine = this.taskView.getLine();
        final Integer index = Integer.parseInt(indexLine) - 1;
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Task updatedTask = this.taskService.updateByIndex(index, title, description);
        this.taskView.print(updatedTask);
    }

    @Override
    public void updateById() {
        final String id = this.taskView.getLine();
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Task updatedTask = this.taskService.updateById(id, title, description);
        this.taskView.print(updatedTask);
    }

    @Override
    public void removeByIndex() {
        final String indexLine = this.taskView.getLine();
        final Integer index = Integer.parseInt(indexLine) - 1;
        final Task task = this.taskService.removeByIndex(index);
        this.taskView.print(task);
    }

    @Override
    public void removeById() {
        final String id = this.taskView.getLine();
        final Task task = this.taskService.getById(id);
        this.taskView.print(task);
    }

    @Override
    public void removeByTitle() {
        final String title = this.taskView.getLine();
        final Task task = this.taskService.removeByTitle(title);
        this.taskView.print(task);
    }

    @Override
    public void removeAll() {
        this.taskService.removeAll();
        this.taskView.print(NotifyConst.SUCCESS_MSG);
    }

    @Override
    public void viewByIndex() {
        final String indexLine = this.taskView.getLine();
        final Integer index = Integer.parseInt(indexLine) - 1;
        final Task task = this.taskService.getByIndex(index);
        this.taskView.print(task);
    }

    @Override
    public void viewById() {
        final String id = this.taskView.getLine();
        final Task task = this.taskService.getById(id);
        this.taskView.print(task);
    }

}