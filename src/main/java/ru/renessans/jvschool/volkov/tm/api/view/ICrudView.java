package ru.renessans.jvschool.volkov.tm.api.view;

import java.util.List;

public interface ICrudView<T> {

    String getLine();

    void print(List<T> objects);

    void print(T object);

    void print(String printable);

    void printFail();

}