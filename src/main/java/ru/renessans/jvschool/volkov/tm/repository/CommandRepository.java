package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.model.Command;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.*;

public final class CommandRepository implements ICommandRepository {

    private final Map<CommandType, List<Command>> commands = new HashMap<>();

    @Override
    public void addCommandTypes(final CommandType... commandTypes) {
        if (Objects.isNull(commandTypes)) return;

        for (CommandType commandType : commandTypes) {
            this.commands.put(commandType, new ArrayList<>());
        }
    }

    @Override
    public void addCommands(final CommandType commandType, final Command... commands) {
        if (Objects.isNull(commandType)) return;
        if (isNoCommandType(commandType)) return;

        final List<Command> commandList = this.commands.get(commandType);
        commandList.addAll(Arrays.asList(commands));
    }

    @Override
    public List<Command> getCommandsByType(final CommandType commandType) {
        if (Objects.isNull(commandType)) return new ArrayList<>();

        if (commandType.isCommon()) return getCommandsList(CommandType.COMMON);
        if (commandType.isArgument()) return getCommandsList(CommandType.ARG);
        if (commandType.isCommand()) return getCommandsList(CommandType.CMD);
        return new ArrayList<>();
    }

    @Override
    public String getNotifyByType(final CommandType commandType, final String command) {
        if (Objects.isNull(commandType)) return null;
        if (ValidRuleUtil.isNullOrEmpty(command)) return null;

        final List<Command> commands = getCommandsByType(commandType);
        if (ValidRuleUtil.isNullOrEmpty(commands)) return null;

        for (Command cmd : commands) {
            if (Objects.equals(commandByType(commandType, cmd), command))
                return cmd.getNotification();
        }
        return null;
    }

    private boolean isNoCommandType(final CommandType commandType) {
        return this.commands.get(commandType) == null;
    }

    private String commandByType(final CommandType commandType, final Command command) {
        if (Objects.isNull(commandType)) return null;
        if (Objects.isNull(command)) return null;

        if (commandType.isArgument()) return command.getArgument();
        if (commandType.isCommand()) return command.getCommand();
        return null;
    }

    private List<Command> getCommandsList(final CommandType commandType) {
        if (Objects.isNull(commandType)) return new ArrayList<>();
        if (isNoCommandType(commandType)) return new ArrayList<>();

        return commands.get(commandType);
    }

}