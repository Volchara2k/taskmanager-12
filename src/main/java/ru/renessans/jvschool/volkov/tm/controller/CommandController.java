package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.ICommandController;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.api.view.ICommandView;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    private final ICommandView commandView;

    public CommandController(final ICommandService commandService, final ICommandView commandView) {
        this.commandService = commandService;
        this.commandView = commandView;
    }

    @Override
    public void registrationCommandTypes() {
        this.commandService.addCommandTypes();
        this.commandView.printNotification(NotifyConst.COMMAND_TYPES_REGISTRATION_MSG);
    }

    @Override
    public void registrationCommands() {
        this.commandService.addCommonCommands();
        this.commandService.addTerminalCommands();
        this.commandService.addArgumentCommands();
        this.commandView.printNotification(NotifyConst.COMMANDS_REGISTRATION_MSG);
    }

    @Override
    public void registrationNotifyCommands() {
        this.commandService.addNotifyCommonCommands();
        this.commandService.addNotifyTerminalCommands();
        this.commandService.addNotifyArgumentCommands();
        this.commandView.printNotification(NotifyConst.NOTIFICATION_REGISTRATION_MSG);
    }

    @Override
    public void printCommandNotify(final String command) {
        String notify = this.commandService.getTerminalCommandNotify(command);
        this.commandView.printNotification(notify);
    }

    @Override
    public void printArgumentNotify(final String command) {
        String notify = this.commandService.getArgumentCommandNotify(command);
        this.commandView.printNotification(notify);
    }

}