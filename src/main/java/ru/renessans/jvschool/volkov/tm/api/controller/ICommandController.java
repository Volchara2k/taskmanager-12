package ru.renessans.jvschool.volkov.tm.api.controller;

public interface ICommandController {

    void registrationCommandTypes();

    void registrationCommands();

    void registrationNotifyCommands();

    void printCommandNotify(String command);

    void printArgumentNotify(String command);

}