package ru.renessans.jvschool.volkov.tm.constant;

public interface NotifyConst {

    String FORMAT_MSG_UNKNOWN = "Неизвестная команда: %s\n";

    String FORMAT_MSG_HELP = "%s";

    String FORMAT_MSG_ARG = "%s";

    String FORMAT_MSG_CMD = "%s";

    String FORMAT_MSG_INFO =
            "Доступные процессоры (ядра): %d; \n" +
                    "Свободная память: %s; \n" +
                    "Максимальная память: %s; \n" +
                    "Общая память, доступная JVM: %s; \n" +
                    "Используемая память JVM: %s.";

    String FORMAT_MSG_ABOUT = "%s - разработчик; \n%s - почта.";

    String VERSION_MSG = "Версия: 1.0.12.";

    String DEV_MSG = "Valery Volkov";

    String DEV_MAIL_MSG = "volkov.valery2013@yandex.ru";

    String EXIT_MSG = "Выход из приложения!";

    String NO_COMMAND_MSG = "Входная команда отсутствует!";

    String NO_COMMAND_TYPE_MSG = "Не указан тип команды!";

    String ADD_LINE_MSG = "Введите информацию для внесения изменений: ";

    String SUCCESS_MSG = "Успешно!\n";

    String TASK_CREATE_MSG = "Для создания задачи введите её заголовок или заголовок с описанием.";

    String TASK_LIST_MSG = "Текущий список задач: ";

    String TASK_CLEAR_MSG = "Производится очистка списка задач...";

    String TASK_UPDATE_BY_INDEX_MSG = "Для обновления задачи по индексу введите индекс задачи из списка ниже.\n" +
            "Для обновления задачи введите её заголовок или заголовок с описанием.";

    String TASK_UPDATE_BY_ID_MSG = "Для обновления задачи по идентификатору введите идентификатор задачи из списка ниже.\n" +
            "Для обновления задачи введите её заголовок или заголовок с описанием.";

    String TASK_DELETE_BY_INDEX_MSG = "Для удаления задачи по индексу введите индекс задачи из списка ниже.\n";

    String TASK_DELETE_BY_ID_MSG = "Для удаления задачи по идентификатору введите идентификатор задачи из списка ниже.\n";

    String TASK_DELETE_BY_TITLE_MSG = "Для удаления задачи по заголовку введите имя заголовок из списка ниже.\n";

    String TASK_VIEW_BY_INDEX_MSG = "Для отображения задачи по индексу введите индекс задачи из списка ниже.\n";

    String TASK_VIEW_BY_ID_MSG = "Для отображения задачи по идентификатору введите идентификатор задачи из списка ниже.\n";

    String PROJECT_CREATE_MSG = "Для создания проекта введите его заголовок или заголовок с описанием.";

    String PROJECT_LIST_MSG = "Текущий список проектов: ";

    String PROJECT_CLEAR_MSG = "Производится очистка списка проектов...";

    String PROJECT_UPDATE_BY_INDEX_MSG = "Для обновления проекта по индексу введите индекс проекта из списка ниже.\n" +
            "Для обновления проекта введите его заголовок или заголовок с описанием.";

    String PROJECT_UPDATE_BY_ID_MSG = "Для обновления проекта по идентификатору введите идентификатор проекта из списка ниже.\n" +
            "Для обновления проекта введите его заголовок или заголовок с описанием.";

    String PROJECT_DELETE_BY_INDEX_MSG = "Для удаления проекта по индексу введите индекс проекта из списка ниже.\n";

    String PROJECT_DELETE_BY_ID_MSG = "Для удаления проекта по идентификатору введите идентификатор проекта из списка ниже.\n";

    String PROJECT_DELETE_BY_TITLE_MSG = "Для удаления заголовка по имени введите заголовок проекта из списка ниже.\n";

    String PROJECT_VIEW_BY_INDEX_MSG = "Для отображения проекта по индексу введите индекс проекта из списка ниже.\n";

    String PROJECT_VIEW_BY_ID_MSG = "Для отображения проекта по идентификатору введите идентификатор проекта из списка ниже.\n";

    String COMMAND_TYPES_REGISTRATION_MSG = "Типы команд успешно зарегистрированы!";

    String COMMANDS_REGISTRATION_MSG = "Команды успешно зарегистрированы!";

    String NOTIFICATION_REGISTRATION_MSG = "Уведомления для отдельных списков команд успешно зарегистрированы!";

    String EMPTY_TASK_LIST_MSG = "Список задач на текущий момент пуст.\n";

    String EMPTY_PROJECT_LIST_MSG = "Список проектов на текущий момент пуст.\n";

    String FAIL_MSG = "Провалено!";

}