package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.ICrudController;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.model.Project;

import java.util.List;
import java.util.Objects;

public final class ProjectController implements ICrudController {

    private final ICrudService<Project> projectService;

    public final ICrudView<Project> taskView;

    public ProjectController(ICrudService<Project> projectService, ICrudView<Project> projects) {
        this.projectService = projectService;
        this.taskView = projects;
    }

    @Override
    public void list() {
        final List<Project> projects = this.projectService.getAll();
        this.taskView.print(projects);
    }

    @Override
    public void create() {
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        this.projectService.add(title, description);
        this.taskView.print(NotifyConst.SUCCESS_MSG);
    }

    @Override
    public void updateByIndex() {
        final String indexLine = this.taskView.getLine();
        final Integer index = Integer.parseInt(indexLine) - 1;
        final Project project = this.projectService.getByIndex(index);

        if (Objects.isNull(project)) this.taskView.printFail();

        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Project updatedProject = this.projectService.updateByIndex(index, title, description);
        this.taskView.print(updatedProject);
    }

    @Override
    public void updateById() {
        final String id = this.taskView.getLine();
        final Project project = this.projectService.getById(id);

        if (Objects.isNull(project)) {
            this.taskView.printFail();
            return;
        }

        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Project updatedProject = this.projectService.updateById(id, title, description);

        if (Objects.isNull(updatedProject)) {
            this.taskView.printFail();
            return;
        }
        this.taskView.print(NotifyConst.SUCCESS_MSG);
    }

    @Override
    public void removeByIndex() {
        final String indexLine = this.taskView.getLine();
        final Integer index = Integer.parseInt(indexLine) - 1;
        final Project project = this.projectService.getByIndex(index);

        if (Objects.isNull(project)) {
            this.taskView.printFail();
            return;
        }

        this.projectService.removeByIndex(index);
    }

    @Override
    public void removeById() {
        final String id = this.taskView.getLine();
        final Project project = this.projectService.getById(id);

        if (Objects.isNull(project)) {
            this.taskView.printFail();
            return;
        }

        this.projectService.removeById(id);
        this.taskView.print(project);
    }

    @Override
    public void removeByTitle() {
        final String title = this.taskView.getLine();
        final Project project = this.projectService.getById(title);

        if (Objects.isNull(project)) {
            this.taskView.printFail();
            return;
        }

        this.projectService.removeByTitle(title);
        this.taskView.print(project);
    }

    @Override
    public void removeAll() {
        this.projectService.removeAll();
        this.taskView.print(NotifyConst.SUCCESS_MSG);
    }

    @Override
    public void viewByIndex() {
        final String indexLine = this.taskView.getLine();
        final Integer index = Integer.parseInt(indexLine) - 1;
        final Project project = this.projectService.getByIndex(index);
        this.taskView.print(project);
    }

    @Override
    public void viewById() {
        final String id = this.taskView.getLine();
        final Project project = this.projectService.getById(id);
        this.taskView.print(project);
    }

}