package ru.renessans.jvschool.volkov.tm.constant;

public interface CmdConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String INFO = "info";

    String EXIT = "exit";

    String ARGUMENTS = "arguments";

    String COMMANDS = "commands";

    String TASK_CREATE = "task-create";

    String TASK_LIST = "task-list";

    String TASK_CLEAR = "task-clear";

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_DELETE_BY_INDEX = "task-delete-by-index";

    String TASK_DELETE_BY_ID = "task-delete-by-id";

    String TASK_DELETE_BY_TITLE = "task-delete-by-title";

    String TASK_VIEW_BY_INDEX = "task-view-by-index";

    String TASK_VIEW_BY_ID = "task-view-by-id";

    String PROJECT_CREATE = "project-create";

    String PROJECT_LIST = "project-list";

    String PROJECT_CLEAR = "project-clear";

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_DELETE_BY_INDEX = "project-delete-by-index";

    String PROJECT_DELETE_BY_ID = "project-delete-by-id";

    String PROJECT_DELETE_BY_TITLE = "project-delete-by-title";

    String PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String PROJECT_VIEW_BY_ID = "project-view-by-id";

}