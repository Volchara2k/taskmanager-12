package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.model.Command;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public final class CommandService implements ICommandService, NotifyConst {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void addCommandTypes() {
        this.commandRepository.addCommandTypes(
                CommandType.COMMON, CommandType.CMD, CommandType.ARG
        );
    }

    @Override
    public void addCommonCommands() {
        this.commandRepository.addCommands(
                CommandType.COMMON,
                Command.HELP, Command.VERSION, Command.ABOUT, Command.INFO, Command.ARGUMENT, Command.COMMAND,
                Command.PROJECT_CREATE, Command.PROJECT_LIST, Command.PROJECT_CLEAR, Command.PROJECT_UPDATE_BY_INDEX,
                Command.PROJECT_UPDATE_BY_ID, Command.PROJECT_DELETE_BY_INDEX, Command.PROJECT_DELETE_BY_ID,
                Command.PROJECT_DELETE_BY_TITLE, Command.PROJECT_VIEW_BY_INDEX, Command.PROJECT_VIEW_BY_ID,
                Command.TASK_CREATE, Command.TASK_LIST, Command.TASK_CLEAR, Command.TASK_UPDATE_BY_INDEX,
                Command.TASK_UPDATE_BY_ID, Command.TASK_DELETE_BY_INDEX, Command.TASK_DELETE_BY_ID,
                Command.TASK_DELETE_BY_TITLE, Command.TASK_VIEW_BY_INDEX, Command.TASK_VIEW_BY_ID, Command.EXIT
        );
    }

    @Override
    public void addTerminalCommands() {
        this.commandRepository.addCommands(
                CommandType.CMD,
                Command.HELP, Command.VERSION, Command.ABOUT, Command.INFO, Command.ARGUMENT, Command.COMMAND,
                Command.PROJECT_CREATE, Command.PROJECT_LIST, Command.PROJECT_CLEAR, Command.PROJECT_UPDATE_BY_INDEX,
                Command.PROJECT_UPDATE_BY_ID, Command.PROJECT_DELETE_BY_INDEX, Command.PROJECT_DELETE_BY_ID,
                Command.PROJECT_DELETE_BY_TITLE, Command.PROJECT_VIEW_BY_INDEX, Command.PROJECT_VIEW_BY_ID,
                Command.TASK_CREATE, Command.TASK_LIST, Command.TASK_CLEAR, Command.TASK_UPDATE_BY_INDEX,
                Command.TASK_UPDATE_BY_ID, Command.TASK_DELETE_BY_INDEX, Command.TASK_DELETE_BY_ID,
                Command.TASK_DELETE_BY_TITLE, Command.TASK_VIEW_BY_INDEX, Command.TASK_VIEW_BY_ID, Command.EXIT
        );
    }

    @Override
    public void addArgumentCommands() {
        this.commandRepository.addCommands(
                CommandType.ARG,
                Command.HELP, Command.VERSION, Command.ABOUT,
                Command.INFO, Command.ARGUMENT, Command.COMMAND
        );
    }

    @Override
    public void addNotifyCommonCommands() {
        final AtomicReference<String> commonNotify = new AtomicReference<>("");
        final List<Command> commons = this.commandRepository.getCommandsByType(CommandType.COMMON);
        commons.forEach(command -> {
            commonNotify.updateAndGet(v -> v + command.toString() + "\n");
            Command.HELP.setNotification(String.format(NotifyConst.FORMAT_MSG_HELP, commonNotify.get()));
        });
    }

    @Override
    public void addNotifyTerminalCommands() {
        final AtomicReference<String> argumentNotify = new AtomicReference<>("");
        final List<Command> commands = this.commandRepository.getCommandsByType(CommandType.CMD);
        commands.forEach(command -> {
            argumentNotify.updateAndGet(v -> v + command.getArgument() + "\n");
            Command.ARGUMENT.setNotification(String.format(NotifyConst.FORMAT_MSG_ARG, argumentNotify.get()));
        });
    }

    @Override
    public void addNotifyArgumentCommands() {
        final AtomicReference<String> commandNotify = new AtomicReference<>("");
        final List<Command> arguments = this.commandRepository.getCommandsByType(CommandType.ARG);
        arguments.forEach(command -> {
            commandNotify.updateAndGet(v -> v + command.getCommand() + "\n");
            Command.COMMAND.setNotification(String.format(NotifyConst.FORMAT_MSG_CMD, commandNotify.get()));
        });
    }

    @Override
    public String getTerminalCommandNotify(final String command) {
        if (ValidRuleUtil.isNullOrEmpty(command)) return NO_COMMAND_MSG;

        return notifyByType(CommandType.CMD, command);
    }

    @Override
    public String getArgumentCommandNotify(final String command) {
        if (ValidRuleUtil.isNullOrEmpty(command)) return NO_COMMAND_MSG;

        return notifyByType(CommandType.ARG, command);
    }

    private String notifyByType(final CommandType commandType, final String command) {
        if (Objects.isNull(commandType)) return NO_COMMAND_TYPE_MSG;
        if (ValidRuleUtil.isNullOrEmpty(command)) return NO_COMMAND_MSG;

        String notify = "";
        if (commandType.isCommon())
            notify = this.commandRepository.getNotifyByType(CommandType.COMMON, command);
        if (commandType.isArgument())
            notify = this.commandRepository.getNotifyByType(CommandType.ARG, command);
        if (commandType.isCommand())
            notify = this.commandRepository.getNotifyByType(CommandType.CMD, command);
        if (Objects.isNull(notify)) return String.format(FORMAT_MSG_UNKNOWN, command);
        return notify;
    }

}