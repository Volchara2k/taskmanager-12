package ru.renessans.jvschool.volkov.tm.util;

import java.util.Scanner;

public interface ScannerUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

}