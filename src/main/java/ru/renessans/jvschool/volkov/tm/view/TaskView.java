package ru.renessans.jvschool.volkov.tm.view;

import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskView implements ICrudView<Task>, NotifyConst {

    @Override
    public String getLine() {
        System.out.println(ADD_LINE_MSG);
        return ScannerUtil.nextLine();
    }

    @Override
    public void print(final List<Task> tasks) {
        if (ValidRuleUtil.isNullOrEmpty(tasks)) {
            System.out.println(EMPTY_TASK_LIST_MSG);
            return;
        }
        AtomicInteger index = new AtomicInteger(1);
        tasks.forEach(task -> {
            System.out.print(index + ". " + task);
            index.getAndIncrement();
        });
        System.out.println(SUCCESS_MSG);
    }

    @Override
    public void print(final Task task) {
        if (Objects.isNull(task)) {
            printFail();
            return;
        }
        System.out.printf("%s\nid: %s\n", task, task.getId());
        System.out.println(SUCCESS_MSG);
    }

    @Override
    public void print(final String printable) {
        if (Objects.isNull(printable)) return;
        System.out.println(printable);
    }

    @Override
    public void printFail() {
        System.out.println(FAIL_MSG);
    }

}