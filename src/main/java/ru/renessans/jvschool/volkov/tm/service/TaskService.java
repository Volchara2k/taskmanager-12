package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;

public final class TaskService implements ICrudService<Task> {

    private final ICrudRepository<Task> taskRepository;

    public TaskService(final ICrudRepository<Task> taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void add(final String title) {
        if (ValidRuleUtil.isNullOrEmpty(title)) return;

        final Task task = new Task(title);
        this.taskRepository.add(task);
    }

    @Override
    public void add(final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(title)) return;
        if (ValidRuleUtil.isNullOrEmpty(description)) return;

        final Task task = new Task(title, description);
        this.taskRepository.add(task);
    }

    @Override
    public Task updateByIndex(final Integer index, final String title, final String description) {
        if (Objects.isNull(index) || index < 0) return null;
        final Task task = getByIndex(index);
        if (Objects.isNull(task)) return null;

        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String id, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(id)) return null;
        final Task task = getById(id);
        if (Objects.isNull(task)) return null;

        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return this.taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeById(final String id) {
        if (Objects.isNull(id)) return null;
        return this.taskRepository.removeById(id);
    }

    @Override
    public Task removeByTitle(final String title) {
        if (Objects.isNull(title)) return null;
        return this.taskRepository.removeByTitle(title);
    }

    @Override
    public void removeAll() {
        this.taskRepository.removeAll();
    }

    @Override
    public Task getByIndex(final Integer index) {
        if (Objects.isNull(index) || index < 0) return null;
        return this.taskRepository.getByIndex(index);
    }

    @Override
    public Task getById(final String id) {
        if (Objects.isNull(id)) return null;
        return this.taskRepository.getById(id);
    }

    @Override
    public Task getByTitle(final String title) {
        if (Objects.isNull(title)) return null;
        return this.taskRepository.getByTitle(title);
    }

    @Override
    public List<Task> getAll() {
        return this.taskRepository.getAll();
    }

}