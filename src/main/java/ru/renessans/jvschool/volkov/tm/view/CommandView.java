package ru.renessans.jvschool.volkov.tm.view;

import ru.renessans.jvschool.volkov.tm.api.view.ICommandView;

public class CommandView implements ICommandView {

    @Override
    public void printNotification(final String notify) {
        System.out.println(notify);
    }

}