package ru.renessans.jvschool.volkov.tm.view;

import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class ProjectView implements ICrudView<Project>, NotifyConst {

    @Override
    public String getLine() {
        System.out.println(ADD_LINE_MSG);
        return ScannerUtil.nextLine();
    }

    @Override
    public void print(final List<Project> projects) {
        if (ValidRuleUtil.isNullOrEmpty(projects)) {
            System.out.println(EMPTY_PROJECT_LIST_MSG);
            return;
        }
        AtomicInteger index = new AtomicInteger(1);
        projects.forEach(task -> {
            System.out.println(index + ". " + task);
            index.getAndIncrement();
        });
        System.out.println(SUCCESS_MSG);
    }

    @Override
    public void print(final Project project) {
        if (Objects.isNull(project)) {
            printFail();
            return;
        }
        System.out.printf("%s\nid: %s", project, project.getId());
    }

    @Override
    public void print(final String printable) {
        if (Objects.isNull(printable)) return;
        System.out.println(printable);
    }

    @Override
    public void printFail() {
        System.out.println(FAIL_MSG);
    }

}