package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;

public final class ProjectService implements ICrudService<Project> {

    private final ICrudRepository<Project> projectRepository;

    public ProjectService(final ICrudRepository<Project> projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void add(final String title) {
        if (ValidRuleUtil.isNullOrEmpty(title)) return;

        final Project project = new Project(title);
        this.projectRepository.add(project);
    }

    @Override
    public void add(final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(title)) return;
        if (ValidRuleUtil.isNullOrEmpty(description)) return;

        final Project project = new Project(title, description);
        this.projectRepository.add(project);
    }

    @Override
    public Project updateByIndex(final Integer index, final String title, final String description) {
        if (Objects.isNull(index) || index < 0) return null;
        final Project project = getByIndex(index);
        if (Objects.isNull(project)) return null;

        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(id)) return null;
        final Project project = getById(id);
        if (Objects.isNull(project)) return null;

        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return this.projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeById(final String id) {
        if (Objects.isNull(id)) return null;
        return this.projectRepository.removeById(id);
    }

    @Override
    public Project removeByTitle(final String title) {
        if (Objects.isNull(title)) return null;
        return this.projectRepository.removeByTitle(title);
    }

    @Override
    public void removeAll() {
        this.projectRepository.removeAll();
    }

    @Override
    public Project getByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return this.projectRepository.getByIndex(index);
    }

    @Override
    public Project getById(final String id) {
        if (Objects.isNull(id)) return null;
        return this.projectRepository.getById(id);
    }

    @Override
    public Project getByTitle(final String title) {
        if (Objects.isNull(title)) return null;
        return this.projectRepository.getByTitle(title);
    }

    @Override
    public List<Project> getAll() {
        return this.projectRepository.getAll();
    }

}