package ru.renessans.jvschool.volkov.tm.api.controller;

public interface ICrudController {

    void create();

    void list();

    void updateByIndex();

    void updateById();

    void removeByIndex();

    void removeById();

    void removeByTitle();

    void removeAll();

    void viewByIndex();

    void viewById();

}